<?php
namespace Mailchimp\Value;

class Operation implements \JsonSerializable
{
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';
    const METHOD_GET = 'GET';
    const METHOD_PATCH = 'PATCH';


    /**
     * @var string
     */
    protected $path;

    /**
     * @var string
     */
    protected $method;

    /**
     * @var string
     */
    protected $urlParams;

    /**
     * @var string json body
     */
    protected $body;

    /**
     * @var string
     */
    protected $operationId;

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     * @return $this
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return $this
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrlParams()
    {
        return $this->urlParams;
    }

    /**
     * @param string $urlParams
     * @return $this
     */
    public function setUrlParams($urlParams)
    {
        $this->urlParams = $urlParams;
        return $this;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     * @return $this
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return string
     */
    public function getOperationId()
    {
        return $this->operationId;
    }

    /**
     * @param string $operationId
     * @return $this
     */
    public function setOperationId($operationId)
    {
        $this->operationId = $operationId;
        return $this;
    }

    /**
     * @inheritdoc
     */
    function jsonSerialize()
    {
        return array_filter([
            'path'          => $this->getPath(),
            'body'          => $this->getBody(),
            'method'        => $this->getMethod(),
            'url_params'    => $this->getUrlParams(),
            'operation_id'  => $this->getOperationId()
        ]);
    }
}
