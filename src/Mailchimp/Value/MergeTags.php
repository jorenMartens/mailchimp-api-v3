<?php
namespace Mailchimp\Value;

class MergeTags implements \JsonSerializable
{
    protected $map = [];

    public function __construct($mergeTags = [])
    {
        $this->setMergeTags($mergeTags);
    }

    /**
     * replaces the current stored merge tags
     *
     * @param array $mergeTags assoc array of merge tags
     * @return $this
     */
    public function setMergeTags(array $mergeTags)
    {
        $this->resetMergeTags();
        $this->addMergeTags($mergeTags);
        return $this;
    }

    public function resetMergeTags()
    {
        $this->map = [];
    }

    /**
     * @param string $key
     * @param string $name
     * @return $this
     */
    public function addMergeTag($key, $name)
    {
        $this->map[strtoupper($key)] = $name;
        return $this;
    }

    /**
     * adds to the current stored merge tags
     *
     * @param array $mergeTags assoc array of merge tags
     * @return $this
     */
    public function addMergeTags(array $mergeTags)
    {
        foreach ($mergeTags as $key => $name) {
            $this->addMergeTag($key, $name);
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getMergeTags()
    {
        return $this->map;
    }

    /**
     * @inheritdoc
     */
    function jsonSerialize()
    {
        return $this->getMergeTags();
    }
}
