<?php
namespace Mailchimp\Endpoint;

use Mailchimp\Http\Client;
use Mailchimp\Http\Request;

abstract class AbstractEndpoint
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }
}
