<?php
namespace Mailchimp\Endpoint;

use Mailchimp\Entity\Member;
use Mailchimp\Entity\SubscriberList;
use Mailchimp\Http\Request;

class Lists extends AbstractEndpoint
{
    /**
     * @link http://developer.mailchimp.com/documentation/mailchimp/reference/lists/#create-post_lists
     *
     * @param SubscriberList $list
     * @return Request
     */
    public function create(SubscriberList $list)
    {
        $request = $this->client->createRequest('/lists', 'POST', $list);
        return $request;
    }

    /**
     * @link http://developer.mailchimp.com/documentation/mailchimp/reference/lists/#read-get_lists
     *
     * @return Request
     */
    public function getAll()
    {
        $request = $this->client->createRequest('/lists', 'GET');
        return $request;
    }

    // sub-resource members

    /**
     * @param $listId
     * @param Member $member
     * @param bool $updateIfExists
     * @return Request
     */
    public function addMember($listId, Member $member, $updateIfExists = false)
    {
        $method = 'POST';
        $uri = '/lists/' . $listId . '/members';
        if ($updateIfExists) {
            $method = 'PUT';
            $uri .= '/' . $this->getSubscribersHash($member->getEmailAddress());
        }
        return $this->client->createRequest($uri, $method, [], $member);
    }

    /**
     * @param string $listId
     * @param Member $member
     * @return Request
     */
    public function updateMember($listId, Member $member)
    {
        $method = 'PATCH';
        $uri = '/lists/' . $listId . '/members/' . $this->getSubscribersHash($member->getEmailAddress());
        return $this->client->createRequest($uri, $method, [], $member);
    }

    /**
     * @param string $listId
     * @param array $options
     * @return Request
     */
    public function getMembers($listId, array $options = [])
    {
        return $this->client->createRequest('/lists/' . $listId . '/members', 'GET', $options);
    }

    /**
     * @param string $emailAddress
     * @return string
     */
    private function getSubscribersHash($emailAddress)
    {
        return md5(mb_strtolower($emailAddress));
    }
}
