<?php
namespace Mailchimp\Converter;

interface RequestToOperationConverterAware
{
    /**
     * @param RequestToOperation $requestToOperation
     * @return mixed
     */
    public function setRequestToOperation(RequestToOperation $requestToOperation);
}
