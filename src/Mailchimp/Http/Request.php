<?php
namespace Mailchimp\Http;

use GuzzleHttp\Psr7\Request as GuzzleRequest;
use Mailchimp\Http\Response\AbstractResponse;
use Mailchimp\Http\Response\ErrorResponse;
use Mailchimp\Http\Response\Response;

/**
 * Decorator for the Guzzle Request
 */
class Request
{
    /**
     * @var GuzzleRequest
     */
    protected $request;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @param Client $client
     * @param GuzzleRequest $request
     */
    public function __construct(Client $client, GuzzleRequest $request)
    {
        $this->client = $client;
        $this->request = $request;
    }

    /**
     * @return GuzzleRequest
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param GuzzleRequest $request
     * @return $this
     */
    public function setRequest($request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @return AbstractResponse
     */
    public function send()
    {
        $response = $this->client->send($this);

        if ($response->getStatusCode() !== 200) {
            return new ErrorResponse($response);
        }

        return new Response($response);
    }
}
