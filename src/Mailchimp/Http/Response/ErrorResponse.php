<?php
namespace Mailchimp\Http\Response;

class ErrorResponse extends Response
{
    /**
     * @return string
     */
    public function getDetail()
    {
        return $this->get('detail');
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->get('title');
    }

    /**
     * @return string
     */
    public function getStatusCode()
    {
        return $this->get('status');
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->get('type');
    }

    /**
     * @return string
     */
    public function getErrors()
    {
        return $this->get('errors');
    }
}
