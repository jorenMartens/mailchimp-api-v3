<?php
namespace Mailchimp\Http;

use Mailchimp\Http\Response\ErrorResponse;
use Psr\Http\Message\ResponseInterface;

class RequestTest extends \PHPUnit_Framework_TestCase
{
    protected function getRequest(Client $mailchimpClient)
    {
        $path = '/';
        $guzzleUri = $this->getMockBuilder('GuzzleHttp\Psr7\Uri')
            ->setConstructorArgs([$path])
            ->getMock();
        $guzzleUri->method('getPath')->willReturn('/v3.0/' . ltrim($path, '/'));

        $guzzleRequest = $this->getMockBuilder('GuzzleHttp\Psr7\Request')
            ->setConstructorArgs(['GET', $guzzleUri, [], '', '1.1'])
            ->getMock();

        return new Request($mailchimpClient, $guzzleRequest);
    }


    protected function getClient()
    {
        $guzzleClient = $this->getMockBuilder('GuzzleHttp\Client')
            ->setConstructorArgs([])
            ->getMock();

        $mailchimpClient = $this->getMockBuilder('Mailchimp\Http\Client')
            ->setConstructorArgs([$guzzleClient, ClientTest::TEST_USERNAME, ClientTest::TEST_API_KEY])
            ->getMock();
        return $mailchimpClient;
    }

    protected function getResponse($error = false)
    {
        $response = $this->getMockBuilder(ResponseInterface::class)
            ->getMock();
        $response
            ->method('getStatusCode')
            ->willReturn($error ? 500 : 200);
        return $response;
    }

    public function testRequestSendsItselfWithHelpOfClient()
    {
        $client = $this->getClient();
        $client->expects($this->once())
            ->method('send')
            ->willReturn($this->getResponse());

        $request = $this->getRequest($client);
        $request->send();
    }

    public function testSendMethodReturnsErrorResponseOnErrorStatusCode()
    {
        $client = $this->getClient();
        $client->expects($this->once())
            ->method('send')
            ->willReturn($this->getResponse(true));

        $request = $this->getRequest($client);
        $response = $request->send();

        $this->assertTrue($response instanceof ErrorResponse);
    }

}
