<?php
namespace Mailchimp\Converter;

use Mailchimp\Http\Request;
use PHPUnit_Framework_TestCase;

class RequestToOperationTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var RequestToOperation
     */
    protected $requestToOperation;

    protected function setUp()
    {
        $this->requestToOperation = new RequestToOperation();
    }

    public function testConvertOneRequestToOperation()
    {
        $body = 'an array of lists';
        $method = 'GET';
        $path = '/list';
        $operation = $this->requestToOperation->one($this->mockRequest($path, $method, $body));

        $this->assertEquals($path, $operation->getPath());
        $this->assertEquals($method, $operation->getMethod());
        $this->assertEquals($body, $operation->getBody());
    }

    public function testConvertManyRequestsToOperations()
    {
        $body = 'an array of lists';
        $method = 'GET';
        $path = '/list';

        $request1 = $this->mockRequest($path, $method, $body);
        $request2 = $this->mockRequest($path, $method, $body);

        $operations = $this->requestToOperation->many([$request1, $request2]);

        foreach ($operations as $operation) {
            $this->assertEquals($path, $operation->getPath());
            $this->assertEquals($method, $operation->getMethod());
            $this->assertEquals($body, $operation->getBody());
        }

        $this->assertEquals(2, count($operations));
    }

    /**
     * @param string $path
     * @param string $method
     * @param string $body
     * @return Request
     */
    private function mockRequest($path, $method, $body)
    {
        $guzzleUri = $this->getMockBuilder('GuzzleHttp\Psr7\Uri')
            ->setConstructorArgs([$path])
            ->getMock();
        $guzzleUri->method('getPath')->willReturn('/v3.0/' . ltrim($path, '/'));

        $guzzleStreamInterface = $this->getMockBuilder('Psr\Http\Message\StreamInterface')->getMock();
        $guzzleStreamInterface->method('getContents')->willReturn($body);

        $guzzleRequest = $this->getMockBuilder('GuzzleHttp\Psr7\Request')
            ->setConstructorArgs([$method, $guzzleUri, [], $body, '1.1'])
            ->getMock();
        $guzzleRequest->method('getUri')->willReturn($guzzleUri);
        $guzzleRequest->method('getMethod')->willReturn($method);
        $guzzleRequest->method('getBody')->willReturn($guzzleStreamInterface);

        $guzzleClient = $this->getMockBuilder('GuzzleHttp\Client')
            ->setConstructorArgs([])
            ->getMock();

        $client = $this->getMockBuilder('Mailchimp\Http\Client')
            ->setConstructorArgs([$guzzleClient, 'username', 'apiKey'])
            ->getMock();

        $mailchimpRequest = $this->getMockBuilder('Mailchimp\Http\Request')
            ->setConstructorArgs([$client, $guzzleRequest])
            ->getMock();



        $mailchimpRequest->method('getRequest')->willReturn($guzzleRequest);

        return $mailchimpRequest;
    }
}
