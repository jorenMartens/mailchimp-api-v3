<?php


// create factory with username and apikey

$factory = new \Mailchimp\Factory('foo', 'xxxssssss-xx');

/** @var Mailchimp\Endpoint\Lists $listEndpoint */
$listEndpoint = $factory->build(\Mailchimp\Factory::LISTS);

$requests = [];

// collect all members you want to add
$member = (new \Mailchimp\Entity\Member())
    ->setEmailAddress('foo@example.org')
    ->setMergeFields(new \Mailchimp\Value\MergeTags([
        'FNAME' => 'foo'
    ]));

$requests[] = $listEndpoint->addMember('xxx', $member, true);

$member = (new \Mailchimp\Entity\Member())
    ->setEmailAddress('foo@example.org')
    ->setMergeFields(new \Mailchimp\Value\MergeTags([
        'FNAME' => 'foo'
    ]));
$requests[] = $listEndpoint->addMember('xxx', $member, true);

/** @var Mailchimp\Endpoint\Batches $bachEndpoint */
$bachEndpoint = $factory->build(\Mailchimp\Factory::BATCHES);

$response = $bachEndpoint->addBatchFromRequests($requests)->send();

if ($response instanceof \Mailchimp\Http\Response\ErrorResponse) {
    // do something when Mailchimp returned an error
} else {
    // do something when the batch is successfully created
}
